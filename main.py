from app import app
from flask import jsonify
from flask import request
from apps.user.user import User
from apps.test.test import Test
cursor = None
user_obj = User()
test_obj = Test()

@app.route('/add', methods=['POST'])
def add_user():

    return user_obj.add_user(request)


@app.route('/users')
def users():

    return user_obj.get_all_users()


@app.route('/user/<int:_id>')
def user(_id):
    return user_obj.get_user_by_id(_id)


@app.route('/update', methods=['PATCH'])
def update_user():

    return user_obj.update_specific_user(request)


@app.route('/delete/<int:id>', methods=["DELETE"])
def delete_user(_id):
    return user_obj.delete_user(_id)


@app.route('/testing')
def my_test():

    return test_obj.test_method()


@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + str(error),
    }
    resp = jsonify(message)
    resp.status_code = 404
    return resp


@app.errorhandler(500)
def not_found(error=None):
    message = {
        'status': 500,
        'message': 'Server Error: ' + str(error),
    }
    resp = jsonify(message)
    resp.status_code = 500
    return resp


if __name__ == "__main__":
    app.run()
