import pymysql
from app import app
from db_config import mysql
from flask import jsonify
from flask import flash, request
from werkzeug import generate_password_hash, check_password_hash

cursor = None


class User:
    def add_user(self, request):
        try:
            _json = request.json
            _name = _json['name']
            _email = _json['email']
            _password = _json['pwd']
            # validate the received values
            if _name and _email and _password and request.method == 'POST':
                # do not save password as a plain text
                # _hashed_password = generate_password_hash(_password)
                _hashed_password = _password
                # save edits
                sql = "INSERT INTO users(user_name, user_email, user_password) VALUES(%s, %s, %s)"
                data = (_name, _email, _hashed_password,)
                conn = mysql.connect()
                cursor = conn.cursor()
                cursor.execute(sql, data)
                conn.commit()
                resp = jsonify('User added successfully!')
                resp.status_code = 200
                cursor.close()
                conn.close()
                return resp
            else:
                return "not_found()"
        except Exception as e:
            return {"Error:": "Invalid data !"}

    def get_all_users(self):
        try:
            conn = mysql.connect()
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            cursor.execute("SELECT * FROM users")
            rows = cursor.fetchall()
            print(rows)
            resp = jsonify(rows)
            resp.status_code = 200
            cursor.close()
            conn.close()
            return resp
        except Exception as e:
            print(e)
            return {"Error:": "An Error Occurred"}

    def get_user_by_id(self, _id):
        try:
            conn = mysql.connect()
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            cursor.execute("SELECT * FROM users WHERE user_id=%s", _id)
            row = cursor.fetchone()
            resp = jsonify(row)
            resp.status_code = 200
            cursor.close()
            conn.close()
            return resp
        except Exception as e:
            print(e)
            return {"Error:": "An Error Occurred"}

    def update_specific_user(self, request):
        try:
            _json = request.json
            _id = _json['id']
            _name = _json['name']
            _email = _json['email']
            _password = _json['pwd']
            # validate the received values
            if _name and _email and _password and _id and request.method == 'PATCH':
                # do not save password as a plain text
                # _hashed_password = generate_password_hash(_password)
                # save edits
                sql = "UPDATE users SET user_name=%s, user_email=%s, user_password=%s WHERE user_id=%s"
                data = (_name, _email, _password, _id,)
                conn = mysql.connect()
                cursor = conn.cursor()
                cursor.execute(sql, data)
                conn.commit()
                resp = jsonify('User updated successfully!')
                resp.status_code = 200
                cursor.close()
                conn.close()
                return resp
            else:
                return "NOT FOUND"
        except Exception as e:
            print(e)
            return {"Error:": "An Error Occurred"}

    def delete_user(self, _id):
        try:
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.execute("DELETE FROM users WHERE user_id=%s", (_id,))
            conn.commit()
            resp = jsonify('User deleted successfully!')
            resp.status_code = 200
            cursor.close()
            conn.close()
            return resp
        except Exception as e:
            print(e)
            return {"Error:": "An Error Occurred"}

