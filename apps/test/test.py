import pymysql
from db_config import mysql
from flask import jsonify
cursor = None


class Test:
    def test_method(self):
        try:
            conn = mysql.connect()
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            cursor.execute("SELECT * FROM users")
            rows = cursor.fetchall()
            print(rows)
            resp = jsonify(rows)
            resp.status_code = 200
            cursor.close()
            conn.close()
            return resp
        except Exception as e:
            print(e)
            return {"Error:": "An Error Occurred"}
